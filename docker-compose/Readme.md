# Vault setup

Links
- https://hub.docker.com/_/vault
- https://github.com/hashicorp/docker-vault

Resources:
- https://www.bogotobogo.com/DevOps/Docker/Docker-Vault-Consul.php
- https://blog.ruanbekker.com/blog/2019/05/06/setup-hashicorp-vault-server-on-docker-and-cli-guide

## Starting up

Start this container by running this script 

How can I add secrets? 

First, we want to initialize the vault, unseal it and then login.

Start the Vault by executing the [docker-compose.up.bat](docker-compose.up.bat) script. Beware, this script will delete/reset the Vault every time.

Go to => http://localhost:8200

### Initalize the vault 
```
vault operator init
```
This will by default create 5 key shares and a key threshold of 3 keys.
Use 3 of the keys to UNSEAL the Vault.

Example:
```
/ # vault operator init
Unseal Key 1: 6++9Q3np0lKwW+IZMtBwMPCpvNR1v69H6vvzweIHeo2g
Unseal Key 2: t4YIz+EeHJBnvBKSJlm+zrp4zO0vDqdznmtlroPXzUDN
Unseal Key 3: 4Rz477nwS5A9caLCGzQu156rNEjK/Gej/qoC0+LG8UON
Unseal Key 4: kSWC+dmNNR+7y6ctiBJI5CzRhymJsJBM8rrn7Gv+r197
Unseal Key 5: OJgaOQtVXkJm3W6fzN6MbRobHsJZ+jEDv8eBu3ZQeqJS

Initial Root Token: s.dOGLGy9lV992bCdhpixFmU3L

Vault initialized with 5 key shares and a key threshold of 3. Please securely
distribute the key shares printed above. When the Vault is re-sealed,
restarted, or stopped, you must supply at least 3 of these keys to unseal it
before it can start servicing requests.

Vault does not store the generated master key. Without at least 3 key to
reconstruct the master key, Vault will remain permanently sealed!

It is possible to generate new unseal keys, provided you have a quorum of
existing unseal keys shares. See "vault operator rekey" for more information.
```

### To unseal the vault, use:
```
vault operator unseal 6++9Q3np0lKwW+IZMtBwMPCpvNR1v69H6vvzweIHeo2g
vault operator unseal t4YIz+EeHJBnvBKSJlm+zrp4zO0vDqdznmtlroPXzUDN
vault operator unseal 4Rz477nwS5A9caLCGzQu156rNEjK/Gej/qoC0+LG8UON
```

### Login into the vault using:
```
vault login s.dOGLGy9lV992bCdhpixFmU3L
```

### Then we need to have somewhere to add the secrets. Enable the secret kv engine:
```
vault secrets enable -version=1 -path=secret kv
```

### Add a secret
```
vault kv put secret/my-app/password password=123
```
And read it like so:
```
vault kv get secret/my-app/password
```

## Script/Batch

Use these example scripts against the container

```
vault operator unseal 6++9Q3np0lKwW+IZMtBwMPCpvNR1v69H6vvzweIHeo2g
vault operator unseal t4YIz+EeHJBnvBKSJlm+zrp4zO0vDqdznmtlroPXzUDN
vault operator unseal 4Rz477nwS5A9caLCGzQu156rNEjK/Gej/qoC0+LG8UON
vault login s.dOGLGy9lV992bCdhpixFmU3L
vault secrets enable -version=1 -path=secret kv
vault kv put secret/my-app/password password=123
vault kv get secret/my-app/password

```
