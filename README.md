# A variant of the HashiCorp's Docker Hub for Vault

The Dockerfile is without the VOLUME /data/files to make it possible to use "docker commit" and reuse the Docker Image.

Build the Docker Image on Windows using the script:  [docker.build.bat](docker.build.bat)

Start a container by running:  [docker-compose.up.bat](docker-compose\docker-compose.up.bat)

Access Vault at: http://localhost:8200

Visit Docker Hub: https://hub.docker.com/repository/docker/geircode/hashicorp-vault


## Orginal docs

The version of this hosted on [HashiCorp's Docker Hub for Vault](https://hub.docker.com/r/hashicorp/vault/)
is built from the same source as the [Vault Official Image](https://hub.docker.com/_/vault/).

There are several pieces that are used to build this image:

* We start with an Alpine base image and add CA certificates in order to reach
  the HashiCorp releases server. These are useful to leave in the image so that
  the container can access Atlas features as well.
* Finally a specific Vault build is fetched and the rest of the Vault-specific
  configuration happens according to the Dockerfile.
